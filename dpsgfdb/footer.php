	<div id="footer">
		<div class="desktop-only">
			<?php if ( is_active_sidebar( 'footer_widget' ) ) : ?>
			<div class="widget-area" role="complementary">
				<?php dynamic_sidebar( 'footer_widget' ); ?>
			</div><!-- #primary-sidebar -->
			<?php endif; ?>
		</div>
		<div class="mobile-only" style="padding: 2px 0;">
		<?php if ( is_active_sidebar( 'footer_widget_mobile' ) ) : ?>
			<div class="widget-area" role="complementary">
				<?php dynamic_sidebar( 'footer_widget_mobile' ); ?>
			</div><!-- #primary-sidebar -->
			<?php endif; ?>
		</div>
	</div><!-- footer -->
	</div><!-- content -->
</div><!-- wrapper -->
<?php wp_footer(); ?>
</body>
</html>

<?php get_header(); ?>
  
   <div id="main">
      <?php if (have_posts()) : ?>
         <p class="info">Dein Suchergebnis f&uuml;r <strong><?php echo $s ?></strong>:</p>
  
         <?php while (have_posts()) : the_post(); ?>
		<div class="completepost">
			<div class="posthead">	
				<div class="starticon"></div><!-- starticon-->
   				<h2 class="postheading"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
				<span class="entry-date"><?php echo get_the_date(); ?></span>
				<div class="floatstop"></div>
			</div><!--posthead-->
	   		<div class="entry">
      				<?php the_excerpt(); ?>
      			</div><!-- entry -->
			<div class="floatstop"></div>
			<!--<div class="postfoot">-->
				<div class="zielicon"></div><!--zielicon-->
			<!--</div> postfoot-->
			<div class="postseperator"></div>
		</div><!--completepost-->
         <?php endwhile; ?>
  
      <?php else : ?>
	<div class="posthead">	
		<div class="starticon"></div><!-- starticon-->
   			<h2 class="postheading">Suchergebnis</h2>
			<div class="floatstop"></div>
		</div><!--posthead-->
		<div class="entry">
         		<p class="searchnotfound">Sorry, leider konnten wir f&uuml;r <strong><?php echo $s ?></strong> nichts finden.</p>
		</div><!--entry-->
 	 <div class="zielicon"></div><!--zielicon-->
      <?php endif; ?>
   </div><!-- main -->
  
   <div id="sidebar">
      <?php get_sidebar(); ?>
   </div><!-- sidebar -->  
  <div class="floatstop"></div>
<?php get_footer(); ?>
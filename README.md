Wordpress theme for DPSG websites
=================================

This wordpress theme follows <a href="http://www.dpsg.de/" target=_blank>DPSG's</a> corporate design guidelines.

![sample](/samplescreen.png?raw=true "Sample screenshot")

Requirements:
- [x] Wordpress >= 4.0
- [x] recommended: Responsive Lightbox Plugin (dfactory)

to do:
- [ ] footer needs improvement
- [ ] additional image slideshow?

<!DOCTYPE HTML>
<html>
<head> 
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable= no">
  	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo
('charset'); ?>" />

	<!-- grab ubuntu font from google webfonts repo -->
	<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,700' rel='stylesheet' type='text/css'>
  
	<title><?php wp_title(''); ?></title>
  
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<!-- image slider stuff -->

  
	<?php wp_head(); ?>
</head>
<body>
	<div id="navbar">
		<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
	</div>

<div id="wrapper">
	<div id="linetop"></div>
	<div id="content">
  	<div id="header">
		<a href="<?php bloginfo('url'); ?>" id="logo">DPSG Friedberg Logo</a>
		<form method="get" id="searchform" action="<?php echo $_SERVER['PHP_SELF']; ?>">
   			<input type="text" id="searchinput" name="s" value='Seite durchsuchen' onfocus="if(this.value=='Seite durchsuchen'){this.value='';}" onblur="if(this.value==''){this.value='Seite durchsuchen';}" />
   			<input type="submit" id="searchsubmit" value="" />
		</form>
		<h1 id="siteheading"><?php bloginfo('name'); ?></h1>
		<div class="floatstop"></div>
		<!--<h1><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a></h1>
		<h3><?php bloginfo('description'); ?></h3>-->
		<div class="desktop-only">
			<div class="headerslider">
				<div id="slides" style="overflow: hidden;">
					<img src="/wp-content/uploads/2014/12/lagerplatz2.jpg"  alt="">
	   				<img src="/wp-content/uploads/2015/10/gruppe2.jpg"  alt="">
	   				<img src="/wp-content/uploads/2014/12/natur.jpg"  alt="">
	   				<img src="/wp-content/uploads/2014/12/pause.jpg"  alt="">
	  				<img src="/wp-content/uploads/2014/12/nationen.jpg"  alt="">
	   				<img src="/wp-content/uploads/2014/12/haik3.jpg"  alt="">
				</div>
			</div>
		</div>

	</div><!-- header -->